import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

import {CognitoUserPool, CognitoUserAttribute, CognitoUser} from 'amazon-cognito-identity-js';

import {environment} from '../../../environments/environment';
import {RegisterUser} from '../models/register-user';

@Injectable()
export class RegistrationService {
    private poolData = {
        UserPoolId: environment.UserPoolId,
        ClientId: environment.ClientId
    };
    private userPool = new CognitoUserPool(this.poolData);
    private cognitoUser: CognitoUser;

    constructor(private router: Router) {
    }

    register(registerUser: RegisterUser) {
        const attributeList = [];
        const dataEmail = {
            Name: `email`,
            Value: registerUser.email
        };
        const attributeEmail = new CognitoUserAttribute(dataEmail);

        attributeList.push(attributeEmail);
        this.userPool.signUp(registerUser.phone_number, registerUser.password, attributeList, null, (err, result) => {
            if (err) {
                alert(err);
                return;
            }
            this.cognitoUser = result.user;
            this.router.navigateByUrl(`/auth/registration/confirm`);
        });
    }

    confirmRegistration(confirmationCode: string) {
        this.cognitoUser.confirmRegistration(confirmationCode, true, (err, result) => {
            if (err) {
                alert(err);
                return;
            }
            console.log(`call result: ${result}`);
        });
    }

}
