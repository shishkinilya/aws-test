import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {RegistrationService} from '../services/registration.service';

@Component({
    selector: 'app-confirm',
    templateUrl: './confirm.component.html',
    styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
    confirmForm: FormGroup;
    isSubmitted = false;

    get codeControl() {
        return this.confirmForm.get(`confirmationCode`);
    }

    constructor(private fb: FormBuilder,
                private registrationService: RegistrationService) {
        this.createConfirmForm();
    }

    ngOnInit() {
    }

    createConfirmForm() {
        this.confirmForm = this.fb.group({
            confirmationCode: [``, [Validators.required]]
        });
    }

    onSubmit() {
        this.isSubmitted = true;
    }

    onConfirm() {
        this.registrationService.confirmRegistration(this.codeControl.value);
    }

}
