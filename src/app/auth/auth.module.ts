import {NgModule} from '@angular/core';

import {SharedModule} from '../shared/shared.module';
import {AuthRoutingModule} from './auth-routing.module';
import {RegistrationComponent} from './registration/registration.component';
import {RegistrationService} from './services/registration.service';
import { ConfirmComponent } from './confirm/confirm.component';

@NgModule({
    imports: [
        AuthRoutingModule,
        SharedModule
    ],
    declarations: [RegistrationComponent, ConfirmComponent],
    providers: [RegistrationService]
})
export class AuthModule {
}
