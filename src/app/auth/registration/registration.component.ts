import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {RegistrationService} from '../services/registration.service';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
    regForm: FormGroup;
    isSubmitted = false;

    get passwordControl() {
        return this.regForm.get(`password`);
    }

    get phoneNumberControl() {
        return this.regForm.get(`phone_number`);
    }

    get emailControl() {
        return this.regForm.get(`email`);
    }

    constructor(private fb: FormBuilder,
                private registrationService: RegistrationService) {
        this.createRegForm();
    }

    ngOnInit() {
    }

    createRegForm() {
        this.regForm = this.fb.group({
            email: [``, [Validators.required]],
            phone_number: [``, [Validators.required]],
            password: [``, [Validators.required, Validators.minLength(6)]]
        });
    }

    onRegister() {
        this.registrationService.register(this.regForm.value);
    }

    onSubmit() {
        this.isSubmitted = true;
    }

}
